from screen import Screen


class Figures(object):
    list_of_figures = []


class Figure:
    def __init__(self, param):
        self.ftype = param["type"]
        self.param = param
        if "color" in param:
            self.color = param["color"]
        else:
            self.color = Screen.fg_default

    def factory(figure):
        if figure.ftype == "circle":
            return Circle(figure)
        if figure.ftype == "point":
            return Point(figure)
        if figure.ftype == "rectangle":
            return Rectangle(figure)
        if figure.ftype == "square":
            figure.param["height"] = figure.param["size"]
            figure.param["width"] = figure.param["size"]
            return Square(figure)
        if figure.ftype == "polygon":
            return Polygon(figure)

    factory = staticmethod(factory)

    def __str__(self):
        return str(self.color) + " " + self.ftype


class Point(Figure):
    def __init__(self, figure):
        super().__init__(figure.param)
        self.x = figure.param["x"]
        self.y = figure.param["y"]

    def draw_figure(self, drawer, palette):
        drawer.point([self.x, self.y], fill=palette.get_colour(self.color))

    def __str__(self):
        return super().__str__() + ": (" + str(self.x) + ", " + str(self.y) + ")"


class Polygon(Figure):
    def __init__(self, figure):
        super().__init__(figure.param)
        self.points = figure.param["points"]

    def draw_figure(self, drawer, palette):
        p = self.get_points_tuple()
        drawer.polygon(p, fill=palette.get_colour(self.color))

    def get_points_tuple(self):
        tup = []
        for xy in self.points:
            tup.append(tuple(xy))
        return tuple(tup)

    def __str__(self):
        return super().__str__() + ": " + str(self.points)


class Rectangle(Figure):
    def __init__(self, figure):
        super().__init__(figure.param)
        ## wspolrzedne srodka
        self.x = figure.param["x"]
        self.y = figure.param["y"]
        self.width = figure.param["width"]
        self.height = figure.param["height"]

    def draw_figure(self, drawer, palette):
        border = self.find_border()
        drawer.rectangle(border, fill=palette.get_colour(self.color))

    def find_border(self):
        result = ((self.x - (self.width / 2), self.y - (self.height / 2)),
                  (self.x + (self.width / 2), self.y + (self.height / 2)))
        return result

    def __str__(self):
        return super().__str__() + ": (" + str(self.x) + ", " + str(self.y) + ")"


class Square(Rectangle):
    def __init__(self, figure):
        super().__init__(figure)

    def __str__(self):
        return super().__str__()


class Circle(Figure):
    def __init__(self, figure):
        super().__init__(figure.param)
        self.x = figure.param["x"]
        self.y = figure.param["y"]
        self.radius = figure.param["radius"]

    def draw_figure(self, drawer, palette):
        drawer.ellipse((self.x - self.radius, self.y - self.radius, self.x + self.radius, self.y + self.radius),
                       fill=palette.get_colour(self.color))

    def __str__(self):
        return super().__str__() + ": (" + str(self.x) + ", " + str(self.y) + "), R = " + str(self.radius)
