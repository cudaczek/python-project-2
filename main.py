import sys
import argparse
import json

from figure import Figures, Figure
from screen import Palette, Screen
from PIL import ImageDraw


def main(argv):
    parser = argparse.ArgumentParser(description='Create a painting like Picasso...')
    parser.add_argument('input_file', type=str, help='json input file')
    parser.add_argument('-o', '--output', required=False, help='output file where painting will be saved')
    args = parser.parse_args()
    # print(args.accumulate(args.integers))
    # input_file = 'sample.json'
    if args.input_file is not None:
        with open(args.input_file) as file:
            reader = json.load(file)
    if 'Figures' not in reader or "Palette" not in reader or "Screen" not in reader:
        print("Missing data in JSON file.")
    read_figures = reader["Figures"]
    read_palette = Palette(reader["Palette"])
    read_screen = Screen(reader["Screen"])
    to_show = Figures
    for figure in read_figures:
        to_show.list_of_figures.append(Figure.factory(Figure(figure)))
    drawer = ImageDraw.Draw(read_screen.img)
    for single in to_show.list_of_figures:
        single.draw_figure(drawer, read_palette)
    read_screen.img.show()
    if args.output is not None:
        read_screen.img.save(args.output, "PNG")


if __name__ == "__main__":
    main(sys.argv)
